# HTML5 Banner Quickstart
Author: Tim Santor <tsantor@xstudios.agency>

## Overview

This repo contains the same ad creative executed for 4 popular campaign managers:

- Google DoubleClick Studio
- Google DCM
- Google AdWords
- Sizemek

> Note: Each campaign manager has its own criteria for how ads should be built.


## Ad Validation

You can check your ads for errors against popular target campaign managers here:

- [Google DoubleClick Studio](https://www.google.com/doubleclick/studio)
- [Google DCM](https://h5validator.appspot.com/dcm)
- [Google AdWords](https://h5validator.appspot.com/adwords)
- [Sizemek](https://platform.mediamind.com)
