var container;
var content;
var bgExit;

var ball;
var shadow;
var text1;
var text2;
var text3;
var logo;
var cta;
var divline;
var shine;

// i.e. Giving them Instance Names like in Flash - makes it easier
function assignInstanceNames() {
    // Assign all the elements to the element on the page
    container   = document.getElementById('container_dc');
    content     = document.getElementById('content_dc');
    bgExit      = document.getElementById('background_exit_dc');

    ball        = document.getElementById('ball');
    shadow      = document.getElementById('shadow');
    text1       = document.getElementById('text1');
    text2       = document.getElementById('text2');
    text3       = document.getElementById('text3');
    logo        = document.getElementById('logo');
    cta         = document.getElementById('cta');
    divline     = document.getElementById('divline');
    shine       = document.getElementById('shine');
}

// ---------------------------------------------------------------------------
// ANIMATION
// ---------------------------------------------------------------------------

function animateAd() {
    // frame 1 -2
    TweenLite.from(ball, 1.5, { top:"-=250", ease:Bounce.easeOut});
    TweenLite.from(shadow, 1.5, { top:"+=250", ease:Bounce.easeOut});
    TweenLite.from(text1, .5, { opacity:0, top:"+=10", ease:Sine.easeOut, delay:1.5 });
    TweenLite.from(text2, .5, { opacity:0, top:"+=10", ease:Sine.easeOut, delay:2.5 });

    TweenLite.to(text1, .5, { opacity:0, top:"+=10", ease:Sine.easeOut, delay:4 });
    TweenLite.to(text2, .5, { opacity:0, top:"+=10", ease:Sine.easeOut, delay:4 });

    // frame 3
    TweenLite.from(text3, .5, { opacity:0, top:"+=10", ease:Sine.easeOut, delay:5 });
    TweenLite.to(text3, .5, { opacity:0, top:"+=10", ease:Sine.easeOut, delay:7 });

    // frame 4
    TweenLite.from(logo, .5, { opacity:0, ease:Power0.easeNone, delay:8 });
    TweenLite.from(cta, .5, { opacity:0, scaleX:.5, scaleY:.5, ease:Back.easeOut, delay:8 });
    TweenLite.from(divline, .5, { opacity:0, scaleY:0, ease:Sine.easeOut, delay:8.5 });

    // shine
    TweenLite.to(shine, .5, { backgroundPosition:"150px", ease:Power0.easeNone, delay:9 });
    TweenLite.to(shine, 0, { backgroundPosition:"-150px", ease:Power0.easeNone, delay:9.5 });
}

// ---------------------------------------------------------------------------
// LOAD IMAGES - only animate when images loaded (stops initial load oddities)
// ---------------------------------------------------------------------------

var images = [
    'ball.png', 'shadow.png', 'cta.png',
    'divline.png', 'logo.png', 'text1.png',
    'text2.png', 'text3.png', 'bg.jpg'
];
var index = 0;

// Adwords hack to get around the "Missing Asset Check". If we set the
// source using a method call rather than a string it lets us pass the test.
function getImagePath() {
    return 'images/' + images[index];
}

// load all images before we begin
function loadImage() {
    var img = new Image();
    img.onload = function () {
        ++index;

        if(index < images.length)  {
            loadImage();
            //console.debug('load '+images[index]);
        } else {
            // Animate ad
            animateAd();
            // Show ad (avoids flashing while image assets load)
            container.style.display = "block";
        }
    }
    img.src = getImagePath();
}

// ---------------------------------------------------------------------------
// ADWORDS - typically, we do not mess with this section
// ---------------------------------------------------------------------------

function initEB() {
    startAd();
}

function startAd() {
    assignInstanceNames()
    addEventListeners();
    loadImage();
}

function addEventListeners() {
    bgExit.addEventListener('mouseover', bgOverHandler);
    bgExit.addEventListener('mouseout', bgOutHandler);
    bgExit.addEventListener('click', bgExitHandler);
}

function bgOverHandler(e) {
    //cta.setAttribute("class", "active");
    TweenLite.to(shine, .5, { backgroundPosition:"150px", ease:Power0.easeNone });
}

function bgOutHandler(e) {
    //cta.setAttribute("class", "");
    TweenLite.to(shine, 0, { backgroundPosition:"-150px", ease:Power0.easeNone });
}

function bgExitHandler() {
    console.log('clickthrough');
    ExitApi.exit()
}

window.addEventListener("load", initEB);

// ---------------------------------------------------------------------------
